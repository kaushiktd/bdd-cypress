/// <reference types="Cypress" />
import { ExceptionsHandler } from "../helper/ExceptionsHandler";
const exp = new ExceptionsHandler();

export class OTKLogin{
    
    launchAndNavigate(){
        cy.visit("https://opstoolkit-test.sso.sensormatic.com/develop");
    }

    login(){
        cy.get("#username").type("sso.test411@shoppertrak.com").wait(10000);
        cy.get("button[type='submit']").click();
        exp.classListException();
        cy.get("#password").type("Saml!123");
        cy.get("button[type='submit']").click();
       // exp.classListException();
       cy.wait(5000);
    }

    validateLoginPage(){
        cy.get("span:contains('welcome back!')").should("have.text","welcome back!");
    }
}