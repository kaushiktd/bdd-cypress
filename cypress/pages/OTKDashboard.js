/// <reference types="Cypress" />

export class OTKDashboard{

    getProfileName(){
        
        cy.get("div[class*='user-profile-name']").as('profileName');
        
        cy.intercept('https://opstoolkit-user-management-service-test.sso.sensormatic.com/v1/api/users').as('users');
    }

    logout(){
        cy.get("div[class*='user-profile-name']").click();
        cy.get("button[role*='menuitem']").click();
    }


    dashboardPageIsDisplayed(){

        cy.location().should((loc)=>{
            expect(loc.pathname).to.contain('/develop/');
        })
        cy.wait(10000);
        cy.xpath("//div[@class='user-management-container']").should('be.visible');
    }

    refreshPage(){

        //cy.reload();
    }
}