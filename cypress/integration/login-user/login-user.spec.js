import { OTKDashboard } from "../../pages/OTKDashboard";
import { OTKLogin } from "../../pages/OTKLoginPage";

const loginPage = new OTKLogin();
const dashboardPage = new OTKDashboard();

Given('Launch and navigate to OTK Login page',()=>{
    loginPage.launchAndNavigate();    

})

When('valid user credentials are provided',()=>{
    loginPage.login();
    cy.wait(500);
})

Then('user is successfully logged into OTK',()=>{
    dashboardPage.getProfileName();
    
})

And('username is displayed on top right corner',()=>{
    dashboardPage.logout();
})
