import { OTKDashboard } from "../../pages/OTKDashboard";
import { OTKLogin } from "../../pages/OTKLoginPage";

const loginPage = new OTKLogin();
const dashboardPage = new OTKDashboard();


Given('I am on the opstoolkit portal login page',()=>{
    loginPage.launchAndNavigate(); 
    cy.wait(500);
})

When('I provide internal admin credentials',()=>{
    loginPage.login();
})

Then('I should be able to login',()=>{
    dashboardPage.dashboardPageIsDisplayed();
})