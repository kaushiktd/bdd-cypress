@REQ_OTK-102
Feature: Move Routing to Hash based url in FE for OTK
	#Move Routing to Hash based url in FE for OTK

	#Tests Move Routing to Hash based url in FE for OTK. 
	#
	#As a end user i should be able to refresh page without any error
	@TEST_OTK-104 @TESTSET_OTK-107 @OTK_QA_FE
	Scenario: Test Move Routing to Hash based url in FE for OTK
		Feature:Ops tookkit UI user is able to refresh
		
		
		Background: 
		    Given User is already logged in OPS tool kit portal
		
		Scenario: User is able to refresh 
		    Then validate that dashbaord is displayed
		    When user tried to refresh the page
		    Then page is refreshed without any error
