import { OTKDashboard } from "../../pages/OTKDashboard";
import { OTKLogin } from "../../pages/OTKLoginPage";
import {Given,When,Then,Before,After} from "cypress-cucumber-preprocessor/steps"

const loginPage = new OTKLogin();
const dashboardPage = new OTKDashboard();

Before(()=>{
    console.log("Started");
})

Given('User is already logged in OPS tool kit portal',()=>{
    loginPage.launchAndNavigate(); 
    loginPage.login();
    cy.wait(500);
})

Then('validate that dashbaord is displayed',()=>{
    dashboardPage.dashboardPageIsDisplayed();
})

When('user tried to refresh the page',()=>{
    dashboardPage.refreshPage();
})

Then('page is refreshed without any error',()=>{
    dashboardPage.dashboardPageIsDisplayed();
    //dashboardPage.logout();
})

After(()=>{
    console.log("Done");
})