Feature: Ops tool kit user login

    Role based user login to ops tool kit portal

    Scenario: Sucessful logged in users info should be displayed on right corner

        Given Launch and navigate to OTK Login page
        When valid user credentials are provided
        Then user is successfully logged into OTK 
        And username is displayed on top right corner

        