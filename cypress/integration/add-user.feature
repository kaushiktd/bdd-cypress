Feature: Add Users to OTK

    Users can be added successfully to Ops toolkit portal with different roles

Scenario: Super Admin is able to sucessfully add internal admin users to ops tool kit portal

Given Super admin user is logged into OTK
When Super admin user try to add internal admin
Then Internal User is created in OTK

Scenario Outline: Super Admin is able to successfully add manager user users to ops tool kit portal

Given Super admin user is logged into OTK
When Super admin user try to add "<user>"
Then "<user>" is created 

Examples:
    | user |
    | Customer Admin|
    | Internal Regular|
    | External Regular|


Scenario Outline: Managed user internal admins are not allowed

Given Super admin user is logged into OTK
When Super admin user try to add "<user>"
Then "<user>" is not created 
And Error message is thrown

Examples: 
| user |
| Internal Admin |

